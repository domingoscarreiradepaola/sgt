To run this application, the endpoint with the requested result is : "SGT/GetBestStories".
There is a project test to run unit tests in the solution.
I assumed that the 20 stories was the firsts or random in the ids list.
I developed a singleton semaphore to control the number of concurrent requests on the SGT apis, i assumed that 20 concurrent requests is a fine number for this porpose.
I made an verify after the async loop to ensure the end response after all requests, after run some tests seens to be necessary even using the Task.whenAll.
I also expose other 2 endpoints that its implementation was needed to do and can be useful. (SGT/GetBestIds, SGT/GetStory/{id})