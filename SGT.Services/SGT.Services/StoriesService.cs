﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NLog;
using SGT.Interfaces;
using SGT.Model;
using SGT.Model.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace SGT.Services
{
    public class StoriesService : IStoriesService
    {
        private const int TOTAL_LIST_IDS = 20;
        private const int TOTAL_SECONDS_WAIT_TO_COMPLETE = 10;
        private readonly HttpClient client;
        private IControlSemaphoreService controlSemaphoreService;
        private Logger logger;
        private IOptions<ConfigurationManager> configurationManager;

        public StoriesService(HttpClient client,
            IControlSemaphoreService controlSemaphoreService,
            IOptions<ConfigurationManager> configurationManager)
        {
            this.client = client;
            this.controlSemaphoreService = controlSemaphoreService;
            logger = NLog.LogManager.GetCurrentClassLogger();
            this.configurationManager = configurationManager;
        }
        public async Task<List<int>> GetBestIds()
        {
            await this.controlSemaphoreService.WaitAsync();
            try
            {
                var url = this.configurationManager.Value.BestStoriesIdsUrl;
                var httpResponse = await client.GetAsync(url);

                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new Exception("Cannot retrieve stories");
                }

                var content = await httpResponse.Content.ReadAsStringAsync();
                var tasks = JsonConvert.DeserializeObject<List<int>>(content);

                return tasks;
            }
            finally
            {
                this.controlSemaphoreService.Release();
            }
        }

        public async Task<List<StoryBase>> GetBestStories()
        {
            List<int> lstIds = await this.GetBestIds();
            List<int> lstIdsLimited = lstIds.GetRange(0, TOTAL_LIST_IDS);
            Dictionary<int, StoryBase> stories = new Dictionary<int, StoryBase>();
            var tasks = lstIdsLimited.Select(async item =>
            {
                StoryBase story = null;
                try
                {
                    story = await this.GetById(item);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, ex.Message);
                    story = new StoryEmpty(item);
                    if (!String.IsNullOrEmpty(configurationManager.Value.DefaultErrorMessage))
                    {
                        ((StoryEmpty)story).Message = configurationManager.Value.DefaultErrorMessage.Replace("{exception}", ex.Message);
                    }
                }
                finally
                {
                    if (!stories.Keys.Contains(item))
                    {
                        stories.Add(item, story);
                    }
                }
            });
            await Task.WhenAll(tasks);
            if (stories.Count == TOTAL_LIST_IDS)
            {
                return stories.Values.ToList();
            }

            DateTime dateIniCount = new DateTime();
            while (stories.Count < TOTAL_LIST_IDS)
            {
                await Task.WhenAll(tasks);
                DateTime actualDt = new DateTime();
                var diference = (actualDt - dateIniCount).TotalSeconds;
                if (diference > TOTAL_SECONDS_WAIT_TO_COMPLETE)
                {
                    break;
                }
            }
            return stories.Values.ToList();
        }

        public async Task<Story> GetById(int id)
        {
            await this.controlSemaphoreService.WaitAsync();
            try
            {
                var url = this.configurationManager.Value.StoryDetailUrl.Replace("{id}", id.ToString());
                var httpResponse = await client.GetAsync(url);

                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new Exception("Cannot retrieve stories");
                }

                var content = await httpResponse.Content.ReadAsStringAsync();
                var story = JsonConvert.DeserializeObject<Story>(content);

                return story;
            }
            finally
            {
                this.controlSemaphoreService.Release();
            }
        }
    }
}
