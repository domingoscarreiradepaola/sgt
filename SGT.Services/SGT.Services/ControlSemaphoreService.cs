﻿using SGT.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SGT.Services
{
    public class ControlSemaphoreService : IControlSemaphoreService
    {
        private SemaphoreSlim semaphore;
        private const int CONCURRENT_REQUESTS = 20;
        public ControlSemaphoreService() 
        {
            semaphore = new SemaphoreSlim(CONCURRENT_REQUESTS);
        }
        public async Task WaitAsync() 
        {
            await semaphore.WaitAsync();
        }
        public void Release() 
        {
            semaphore.Release();
        }
    }
}
