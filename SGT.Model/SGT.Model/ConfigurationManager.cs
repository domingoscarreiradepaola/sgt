﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGT.Model
{
    public class ConfigurationManager
    {
        public string BestStoriesIdsUrl { get; set; }
        public string StoryDetailUrl { get; set; }
        public string DefaultErrorMessage { get; set; }
    }
}
