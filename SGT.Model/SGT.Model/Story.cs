﻿using Newtonsoft.Json;
using SGT.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Util;

namespace SGT.Model
{
    public class Story : StoryBase
    {
        private string time;
        private int id;
        private string uri;
        private int commentCount;
        private string postedBy;

        public int Id
        {
            set
            {
                this.id = value;
            }
        }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("PostedBy")]
        public string PostedBy
        {
            get
            {
                return this.postedBy;
            }
        }
        [JsonProperty("By")]
        public string By
        {
            set
            {
                this.postedBy = value;
            }
        }


        [JsonProperty("CommentCount")]
        public int CommentCount
        {
            get
            {
                return this.commentCount;
            }
        }
        [JsonProperty("Descendants")]
        public int Descendants
        {
            set
            {
                this.commentCount = value;
            }
        }

        [JsonProperty("Score")]
        public int Score { get; set; }

        [JsonProperty("Time")]
        public string Time
        {
            get
            {
                return DateUtil.ConvertFromUnixTimestamp(Convert.ToInt32(this.time)).ToString("yyyy-MM-ddTHH:mm:sszzz");
            }
            set
            {
                this.time = value;
            }
        }
        [JsonProperty("Uri")]
        public string Uri
        {
            get
            {
                return this.uri;
            }
        }

        [JsonProperty("Url")]
        public string Url
        {
            set
            {
                this.uri = value;
            }
        }

        public int getId()
        {
            return this.id;
        }
        public override string ToString() 
        {
            return getId().ToString();
        }
    }
}
