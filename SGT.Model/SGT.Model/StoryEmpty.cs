﻿using SGT.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace SGT.Model
{
    public class StoryEmpty : StoryBase
    {
        private int id;
        private string message;
        public const string MESSAGE_EMPTY_DEFAULT = "Something wrong with this record, it´s empty, check the Id and the logs";
        public StoryEmpty(int id)
        {
            this.id = id;
        }
        public int Id
        {
            get { return this.id; }
        }
        public string Message
        {
            get
            {
                if (String.IsNullOrEmpty(this.message)) 
                {
                    return MESSAGE_EMPTY_DEFAULT;
                }
                return this.message;
            }
            set 
            {
                this.message = value;
            }
        }
    }
}
