﻿using SGT.Model;
using SGT.Model.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SGT.Interfaces
{
    public interface IStoriesService
    {
        Task<List<int>> GetBestIds();

        Task<Story> GetById(int id);

        Task<List<StoryBase>> GetBestStories();
    }
}
