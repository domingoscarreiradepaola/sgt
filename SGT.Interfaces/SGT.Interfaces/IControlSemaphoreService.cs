﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SGT.Interfaces
{
    public interface IControlSemaphoreService
    {
        Task WaitAsync();
        void Release();
    }
}
