﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SGT.Interfaces;
using SGT.Model;
using SGT.Model.Base;

namespace SGT.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SGTController : ControllerBase
    {
        IStoriesService storiesService;
        public SGTController(IStoriesService storiesService)
        {
            this.storiesService = storiesService;
        }
        [HttpGet("GetBestIds")]
        public async Task<List<int>> GetBestIds()
        {
            return await storiesService.GetBestIds();
        }
        [HttpGet("GetStory/{id}")]
        public async Task<Story> GetStory(int id)
        {
            return await storiesService.GetById(id);
        }
        [HttpGet("GetBestStories")]
        public async Task<List<StoryBase>> GetBestStories()
        {
            return await storiesService.GetBestStories();
        }
    }
}