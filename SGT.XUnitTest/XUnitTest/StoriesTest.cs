using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SGT.Interfaces;
using SGT.Model;
using SGT.Model.Base;
using SGT.Services;
using System;
using System.Collections.Generic;
using Xunit;

namespace XUnitTest
{
    public class StoriesTest
    {
        IStoriesService storiesService;
        public StoriesTest()
        {
            var services = new ServiceCollection();
            services.UseServices();
            var serviceProvider = services.BuildServiceProvider();
            this.storiesService = serviceProvider.GetRequiredService<IStoriesService>();
        }
        [Fact]
        public async void Test1()
        {
            var result = await this.storiesService.GetBestIds();
            Assert.Equal(200, result.Count);
        }
        [Fact]
        public async void Test2()
        {
            var result = await this.storiesService.GetById(21233041);
            Assert.Equal("ismaildonmez", result.PostedBy);
        }
        [Fact]
        public async void Test3()
        {
            var result = await this.storiesService.GetBestStories();
            Assert.Equal(20, result.Count);
        }
        [Fact]
        public async void Test4()
        {
            List<StoryBase> result = await this.storiesService.GetBestStories();
            var storiesError = result.Find(x => x.GetType() == typeof(StoryEmpty));
            var storiesSuccess = result.FindAll(x => x.GetType() == typeof(Story));
            Assert.True(storiesError == null && storiesSuccess.Count == 20);
        }
    }
}
