﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SGT.Interfaces;
using SGT.Model;
using SGT.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace XUnitTest
{
    public static class Bootstrapper
    {
        public static void UseServices(this IServiceCollection services)
        {
            services.AddHttpClient<IStoriesService, StoriesService>();
            services.AddSingleton<IControlSemaphoreService, ControlSemaphoreService>();
            var builder = new ConfigurationBuilder();
            IConfiguration Configuration = builder.Build();
            services.Configure<ConfigurationManager>(options => 
            {
                options.BestStoriesIdsUrl = "https://hacker-news.firebaseio.com/v0/beststories.json";
                options.StoryDetailUrl = "https://hacker-news.firebaseio.com/v0/item/{id}.json";
            });
        }
    }
}
